import org.apache.flink.api.common.RuntimeExecutionMode;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

/**
 *
 *
 * @Date: 2023/11/15 23:20
 * @Description:
 **/
public class FlinkSinkMysql {
	public static void main(String[] args) throws Exception {
		//准备环境
		StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
		env.setRuntimeMode(RuntimeExecutionMode.AUTOMATIC);
		// 加载数据源
		DataStreamSource<VehicleAlarm> streamSource = env.addSource(new MySource());
		//todo  transformation(数据处理)

		// 数据输出
		streamSource.addSink(new MysqlSink());
		// 程序执行
		env.execute("learn-mysql-sink");
	}



}
