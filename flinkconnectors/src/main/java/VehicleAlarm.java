import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 *
 * @Date: 2023/11/15 23:24
 * @Description:
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
public  class VehicleAlarm {
	private String id;
	private String licensePlate;
	private String plateColor;
	private Long deviceTime;
	private String zone;
}
