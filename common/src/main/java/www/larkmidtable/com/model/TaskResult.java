package www.larkmidtable.com.model;

import lombok.Data;

@Data
public abstract class TaskResult {
    private boolean success;
    private long duration;
}
