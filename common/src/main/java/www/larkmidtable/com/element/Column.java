package www.larkmidtable.com.element;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 *
 * @author fei
 * @date 2023-07-12
 *
 **/
@Accessors(chain = true)
@Data
public class Column implements Serializable {

	private static final long serialVersionUID = 1L;

	private Type type;

	private Object rawData;

	private int byteSize;

	public enum Type {
		BAD, NULL, INT, LONG, DOUBLE, STRING, BOOL, DATE, TIME, DATETIME, BYTES
	}
}
