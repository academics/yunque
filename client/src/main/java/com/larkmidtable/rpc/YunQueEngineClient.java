package com.larkmidtable.rpc;

import com.larkmidtable.yunque.rpc.ClientServiceProxyFactory;
import com.larkmidtable.yunque.rpc.ServerService;
import com.larkmidtable.yunque.rpc.ServerServiceImpl;
import com.larkmidtable.yunque.system.MachineInfo;
import org.apache.commons.cli.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.yaml.snakeyaml.Yaml;
import www.larkmidtable.com.exception.YunQueException;
import www.larkmidtable.com.log.LogRecord;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Date: 2023/8/18 0:17
 * @Description:
 **/
public class YunQueEngineClient {

    private static final Logger logger = LoggerFactory.getLogger(YunQueEngineClient.class);


    public static void main(String[] args) throws Exception {
        LogRecord taskLogRecord = LogRecord.newInstance();
        taskLogRecord.start("云雀数据集成");

        logger.info("Hello! 欢迎使用云雀数据集成....");
        logger.info(MachineInfo.getOSInfo());
        logger.info(MachineInfo.getJAVAInfo());
        // 参数进行校验
        if (args.length == 0) {
//            String projectDir = new File("").getCanonicalPath();
            args = new String[]{"-job",
                    "test",
                    "-jobId",
                    "1",
                    "-path",
                    "D:\\eclipse-workspace\\yunque\\conf\\template\\sqlserver2sqlserver.yaml",
//                    projectDir + "\\conf\\template\\mysql2mysql.yaml",
                    "-fileFormat",
                    "YAML",
                    "-server",
//                    projectDir + "\\conf\\server.yaml"
                    "D:\\eclipse-workspace\\yunque\\conf\\server.yaml",
            };
            logger.warn("尚未传递参数，运行的为默认配置....");
        }

        logger.info("核查参数的正确性....");
        if (args.length != 10) {
            logger.info("程序尚未传递参数，需要传递参数如下:");
            logger.error("例如: " + "\n" + " -job <名称> -jobId <自定作业ID> -path \"<conf目录下的 mysql2tmysql.json 的全路径!!!>\" -fileFormat <作业文件格式 JSON 或者 YAML>" + "\n"
                    + " -job testyunque -jobId testid -path \"D:/yunque/mysql2tmysql.json\" -fileFormat JSON" + "\n");
            throw new YunQueException("运行单体类需要传递参数...");
        }
        logger.info("核查参数的完成....");

        // 获取Server的地址
        String serverPath = args[9];
        BufferedReader br;
        Map<String, Map<String, Object>> jobMap = new HashMap<>();
        try {
            br = new BufferedReader(new FileReader(serverPath));
        } catch (FileNotFoundException e) {
            throw new YunQueException("作业文件路径获取不到，核查参数path的配置....", e);
        }

        Yaml yaml = new Yaml();
        jobMap = yaml.load(br);
        Map<String, Object> server = jobMap.get("server");
        String[] ips = String.valueOf(server.get("location")).split(",");
        // 获取Server拉取的数据量
        String count = String.valueOf(server.get("count"));
        int tcount = Integer.parseInt(count);
        for (int i = 0; i < ips.length; i++) {
            ServerService clientService = (ServerService) ClientServiceProxyFactory.getClientService(ServerService.class, ips[i]);
            logger.info("执行结果:" + clientService.executeSQL(args, i, tcount));
        }
    }
}
